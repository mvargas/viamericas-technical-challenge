'use strict';

App.factory('Service', ['$http', '$q', function($http, $q){

	return {
	
		calculate: function(entrada) {
				return $http.post('http://localhost:8084/calculate/', entrada)
						.then(
								function(response){
									return response.data;
								}, 
								function(errResponse){
									console.error('Error while fetching the calculate'+errResponse);
									return $q.reject(errResponse);
								}
						);
		}
		
	};

}]);
