package com.testsolution.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.testsolution.dto.UserDto;

@Component
public class UserDao  implements Serializable{

	private static final long serialVersionUID = 2932738919808091809L;

	@PersistenceContext
    EntityManager entityManager;
	
	public UserDto findCustomerProductoByCustomerIdAndProducNumber(String username, String password){
		UserDto userDto = null;
		userDto = entityManager.createNamedQuery("userByUsernameAndPassword", UserDto.class)
												.setParameter("username", username).setParameter("password", password)
												.getSingleResult();
        return userDto;
	}
}