package com.testsolution.interfaces;

import com.testsolution.dto.UserDto;
import com.testsolution.exception.UserWebServiceException;

public interface UserService {
	UserDto findUserByUsernameAndPassword(String username, String password) throws UserWebServiceException;
}