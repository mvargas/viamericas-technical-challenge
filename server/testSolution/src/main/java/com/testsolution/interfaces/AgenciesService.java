package com.testsolution.interfaces;

import java.util.List;

import com.testsolution.entity.Agencies;

public interface AgenciesService {
	List<Agencies> findAllAgencies();
}