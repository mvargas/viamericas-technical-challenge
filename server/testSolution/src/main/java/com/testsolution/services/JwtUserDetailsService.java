package com.testsolution.services;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) {
			return new User(username, "$2a$10$P5.TBSs4qgwV5KiYRCqKcuu2zw9X2NulYFTtyyf9aWzFDaexeErm6", new ArrayList<>());
	}
}