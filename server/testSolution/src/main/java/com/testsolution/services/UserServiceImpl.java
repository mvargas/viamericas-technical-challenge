package com.testsolution.services;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testsolution.dao.UserDao;
import com.testsolution.dto.UserDto;
import com.testsolution.exception.UserWebServiceException;
import com.testsolution.interfaces.UserService;


@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;


	@Override
	public UserDto findUserByUsernameAndPassword(String username, String password) throws UserWebServiceException {
        try {
        	return userDao.findCustomerProductoByCustomerIdAndProducNumber(username, password);
        } catch(NoResultException e) {
            throw new UserWebServiceException("User does not exist with " + username+ " and "+password);
        }
		
	}
}
