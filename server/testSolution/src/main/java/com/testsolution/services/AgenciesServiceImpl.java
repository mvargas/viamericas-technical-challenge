package com.testsolution.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testsolution.entity.Agencies;
import com.testsolution.interfaces.AgenciesService;
import com.testsolution.repository.AgenciesRepository;

@Service("AgenciesService")
@Transactional
public class AgenciesServiceImpl implements AgenciesService {
	
	@Autowired
	private AgenciesRepository agenciesRepository;

	@Override
	public List<Agencies> findAllAgencies() {
		return agenciesRepository.findAll();
	}
}
