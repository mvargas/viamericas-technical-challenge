package com.testsolution.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.testsolution.entity.Agencies;

public interface AgenciesRepository extends JpaRepository<Agencies, Long>  {
}