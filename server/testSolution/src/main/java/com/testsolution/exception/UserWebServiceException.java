package com.testsolution.exception;

import java.io.Serializable;

public class UserWebServiceException extends Exception implements Serializable {

	private static final long serialVersionUID = 1169426381288170661L;
	
	public UserWebServiceException() {
	    super();
	}
	
	public UserWebServiceException(String msg) {
	    super(msg);
	}
	
	public UserWebServiceException(String msg, Exception e) {
	    super(msg, e);
	}
}
