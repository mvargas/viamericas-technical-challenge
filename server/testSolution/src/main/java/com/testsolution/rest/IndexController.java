package com.testsolution.rest;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.testsolution.config.JwtTokenUtil;
import com.testsolution.dto.UserDto;
import com.testsolution.entity.Agencies;
import com.testsolution.interfaces.AgenciesService;
import com.testsolution.interfaces.UserService;
import com.testsolution.services.JwtUserDetailsService;

@RestController
@CrossOrigin
public class IndexController {
	
	private static final Logger LOGGER = Logger.getLogger(IndexController.class);
	
	@Autowired
	AgenciesService agenciesService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@GetMapping(value = "/agencies/get", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Agencies>> findAllAgencies(){
		List<Agencies> listaAgencies = agenciesService.findAllAgencies();
        
        if(listaAgencies.isEmpty()){
        	LOGGER.info("You many decide to return HttpStatus.NOT_FOUND");
            return new ResponseEntity<List<Agencies>>(HttpStatus.NO_CONTENT);
        }
        
        return new ResponseEntity<List<Agencies>>(listaAgencies, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody UserDto authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		authenticationRequest.setToken(token);
		
		return new ResponseEntity<UserDto>(authenticationRequest, HttpStatus.OK);
	}
	
	private void authenticate(String username, String password) throws Exception {

		UserDto userDto = null;
		try {
			userDto = userService.findUserByUsernameAndPassword(username, password);
			
			if(null != userDto) {
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			}
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}