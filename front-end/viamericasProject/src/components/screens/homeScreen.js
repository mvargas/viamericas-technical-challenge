import React, { Component } from 'react';
import { AsyncStorage, Button, StyleSheet } from 'react-native';
import Template from './../template/template';
import Header from './../template/header';
import SuggestionList from './suggestion-list';

export default class HomeScreen extends Component {
  //static navigationOptions = { title: 'Welcome to the app!', };
  render() {
    return (
      <Template>
        <Header />
        <SuggestionList />
        <Button
        onPress={this._signOutAsync} title="Salir" />
      </Template>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}
