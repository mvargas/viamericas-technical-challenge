import React, { Component } from 'react';
import { AsyncStorage, FlatList, Text } from 'react-native';
import Layout from './suggestion-list-layout';
import Empty from './empty';
import Separator from './vertical-separator';
import Suggestion from './suggestion';

class SuggestionList extends Component {

  constructor() {
    super();
    this.state = { agencies: [] };
    this._showAgenciesAsync();
  }

  renderEmtpy = () => <Empty text="No hay agencias" />
  itemSeparator = () => <Separator />
  renderItem = ({item}) => {
    return (
      <Suggestion {...item}/>
    )
  }
  render() {
    return (
      <Layout
        title="Agencias"
        >
        <FlatList
          data={this.state.agencies}
          ListEmptyComponent={this.renderEmtpy}
          ItemSeparatorComponent={this.itemSeparator}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => item.id}
        />
      </Layout>
    )
  }

  _showAgenciesAsync = async () => {
    const that = this;
    const userToken = await AsyncStorage.getItem('userToken');
    var object = { method: 'GET', headers: { Authorization: userToken }};
    fetch('https://v45hh4g3q5.execute-api.us-east-1.amazonaws.com/Dev/agencias', object)
    .then(responseData => responseData.json())
    .then(responseData => {
      that.setState({ agencies: responseData });
    }).catch(function(error) {
      Alert.alert('Error', error);
    });
  };
}

export default SuggestionList
