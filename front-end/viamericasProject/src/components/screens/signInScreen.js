import React from 'react';
import {AsyncStorage,Keyboard, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView} from 'react-native';
import { Button } from 'react-native-elements';
import styles from "./../../../style";

export default class SignInScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }
  //static navigationOptions = { title: 'Please sign in', };

  render() {
    return (
      <KeyboardAvoidingView style={styles.containerView} behavior="padding">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.loginScreenContainer}>
          <View style={styles.loginFormView}>
          <Text style={styles.logoText}>Viamericas</Text>
            <TextInput
              placeholder="Username"
              placeholderColor="#c4c3cb"
              style={styles.loginFormTextInput}
              onChangeText={(username) => this.setState({username})}
              value={this.state.username} />
            <TextInput
              placeholder="Password"
              placeholderColor="#c4c3cb"
              secureTextEntry
              style={styles.loginFormTextInput}
              onChangeText={(password) => this.setState({password})}
              value={this.state.password} />
            <Button
              buttonStyle={styles.loginButton}
              onPress={this._signInAsync}
              title="Login"
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }

  _signInAsync = async () => {
    var object = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body:JSON.stringify( {
          "username": this.state.username,
          "password": this.state.password
        })
    };

    fetch('https://v45hh4g3q5.execute-api.us-east-1.amazonaws.com/Dev/authentication', object)
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.status && responseData.status == "success"){
        AsyncStorage.setItem('userToken', responseData.id_token);
        this.props.navigation.navigate('App');
      }else{
        Alert.alert('Error', responseData.errorMessage);
      }
    }).catch(function(error) {
      Alert.alert('Error', error);
    });


  };
}
