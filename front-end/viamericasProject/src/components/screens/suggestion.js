import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

function Suggestion(props) {
  return (
    <View style={[(props.status == 'Open') ? styles.containerClosed : styles.container]}>
      <View style={styles.right}>
        <Text style={styles.title}>{props.name}</Text>
        <Text style={styles.rating}>{props.city} - {props.state} <Text style={styles.status}>{props.status}</Text></Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#C10636',
  },
  containerClosed: {
    flexDirection: 'row',
    backgroundColor: '#70b124',
  },
  right: {
    paddingLeft: 10,
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 20,
    color: 'white'
  },
  status: {
    paddingVertical: 6,
    paddingHorizontal: 6,
    color: 'white',
    fontSize: 11,
    borderRadius: 5,
    overflow: 'hidden',
    alignSelf: 'flex-start'
  },
  rating: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  }

})

export default Suggestion
